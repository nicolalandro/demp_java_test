package com.example.demo_java_test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

import static org.mockito.Mockito.*;


@ExtendWith(ApplicationExtension.class)
public class HomeControllerTest {
    HomeController hc = new HomeController();
    private TextFieldWrapper mTextFieldWrapperMock;
    private LabelWrapper mLabeWrapperlMock;

    @BeforeEach
    void setUp() {
        mTextFieldWrapperMock = mock(TextFieldWrapper.class);
        mLabeWrapperlMock = mock(LabelWrapper.class);
        hc.mTextFieldWrapper = mTextFieldWrapperMock;
        hc.mLabelWrapper = mLabeWrapperlMock;
    }

    @Test
    void onButtonClickDoNotSetTextInLabelIfNullStringTest(){
        when(mTextFieldWrapperMock.getText()).thenReturn("");

        hc.onButtonClick();

        verify(mTextFieldWrapperMock, times(2)).getText();
        verifyNoMoreInteractions(mLabeWrapperlMock);
    }

    @Test
    void onButtonClickSetCorrectTextInLabel(){
        when(mTextFieldWrapperMock.getText()).thenReturn("pippo");

        hc.onButtonClick();

        verify(mTextFieldWrapperMock, times(3)).getText();
        verify(mLabeWrapperlMock, times(1)).setText("pippo");
    }

}
