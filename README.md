[![pipeline status](https://gitlab.com/nicolalandro/demp_java_test/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/demp_java_test/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/demp_java_test/badges/main/coverage.svg)](https://gitlab.com/nicolalandro/demp_java_test/-/commits/main)


# How to run

```
./gradlew --scan
./gradlew build
./gradlew test
./gradlew jacocoTestReport
firefox build/jacocoHtml/index.html

# headless mode
_JAVA_OPTIONS="-Djava.awt.headless=true -Dtestfx.robot=glass -Dtestfx.headless=true -Dprism.order=sw -Dprism.verbose=true" ./gradlew test
```