FROM gradle:jdk11

RUN apt-get update && apt-get install -y xvfb

ADD src /code/src
ADD build.gradle /code/build.gradle
ADD gradlew /code/gradlew
ADD settings.gradle /code/settings.gradle
